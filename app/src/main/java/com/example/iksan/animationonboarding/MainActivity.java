package com.example.iksan.animationonboarding;

import android.animation.Animator;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.eftimoff.viewpagertransformers.ZoomInTransformer;
import com.example.iksan.animationonboarding.adapter.WelcomeAdapter;


public class MainActivity extends AppCompatActivity {

    RelativeLayout rvMaster;
    RelativeLayout rlNext;
    RelativeLayout linDot;
    View redDot;
    View yellowDot;
    View blueDot;
    View greenDot;
    ViewPager viewPager;

    WelcomeAdapter welcomeAdapter;

    int[] colors;
    int[] layouts;
    int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvMaster = (RelativeLayout) findViewById(R.id.activity_main);
        rlNext = (RelativeLayout) findViewById(R.id.rlNext);
        linDot = (RelativeLayout) findViewById(R.id.linDot);
        redDot = (View) findViewById(R.id.redDot);
        yellowDot = (View) findViewById(R.id.yellowDot);
        blueDot = (View) findViewById(R.id.blueDot);
        greenDot = (View) findViewById(R.id.greenDot);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        colors = new int[]{
                R.color.red,
                R.color.yellow,
                R.color.blue,
                R.color.green
        };

        layouts = new int[]{
                R.layout.page_opening,
                R.layout.page_one_item,
                R.layout.page_two_item,
                R.layout.page_three_item,
                R.layout.page_four_item
        };

        welcomeAdapter = new WelcomeAdapter(this, layouts);
        viewPager.setAdapter(welcomeAdapter);
        viewPager.setPageTransformer(true, new ZoomInTransformer());

        rlNext.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (position < colors.length) {
                        Transition transition = TransitionInflater.from(MainActivity.this).inflateTransition(R.transition.changebounds_with_arcmotion);
                        transition.addListener(new Transition.TransitionListener() {
                            @Override
                            public void onTransitionStart(Transition transition) {

                            }

                            @Override
                            public void onTransitionEnd(Transition transition) {
                                switch (position) {
                                    case 0:
                                        redDot.setVisibility(View.INVISIBLE);
                                        break;
                                    case 1:
                                        yellowDot.setVisibility(View.INVISIBLE);
                                        break;
                                    case 2:
                                        blueDot.setVisibility(View.INVISIBLE);
                                        break;
                                    case 3:
                                        greenDot.setVisibility(View.INVISIBLE);
                                        break;
                                }
                                animateRevealColorFromCoordinates(rvMaster, colors[position], (rvMaster.getLeft() + rvMaster.getRight()) / 2, (rvMaster.getTop() + rvMaster.getBottom()) / 2);
                                int currentItem = getItem(+1);
                                if (currentItem < layouts.length) {
                                    viewPager.setCurrentItem(currentItem);
                                }
                            }

                            @Override
                            public void onTransitionCancel(Transition transition) {

                            }

                            @Override
                            public void onTransitionPause(Transition transition) {

                            }

                            @Override
                            public void onTransitionResume(Transition transition) {

                            }
                        });
                        TransitionManager.beginDelayedTransition(rvMaster, transition);

                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(25, 25);
                        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
                        switch (position) {
                            case 0:
                                redDot.setLayoutParams(layoutParams);
                                break;
                            case 1:
                                yellowDot.setLayoutParams(layoutParams);
                                break;
                            case 2:
                                blueDot.setLayoutParams(layoutParams);
                                break;
                            case 3:
                                greenDot.setLayoutParams(layoutParams);
                                break;
                        }
                    }

                }
                return false;
            }
        });


    }


    private Animator animateRevealColorFromCoordinates(ViewGroup viewRoot, @ColorRes int color, int x, int y) {
        float finalRadius = (float) Math.hypot(viewRoot.getWidth(), viewRoot.getHeight());

        Animator anim = ViewAnimationUtils.createCircularReveal(viewRoot, x, y, 0, finalRadius);
        viewRoot.setBackgroundResource(color);
        anim.start();
        position++;
        return anim;
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }


}
